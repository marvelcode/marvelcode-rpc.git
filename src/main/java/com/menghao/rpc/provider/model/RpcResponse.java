package com.menghao.rpc.provider.model;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>Rpc调用响应实体封装.<br>
 *
 * @author MarvelCode.
 */
@Data
public class RpcResponse implements Serializable {
    /*
     * 与RpcRequest中的id对应
     */
    private String id;
    /*
     * 服务调用响应结果
     */
    private Object result;
    /*
     * 服务调用抛出的异常
     */
    private Throwable throwable;

    public RpcResponse(String id, Object result) {
        this.id = id;
        this.result = result;
    }

    public RpcResponse(String id, Throwable throwable) {
        this.id = id;
        this.throwable = throwable;
    }
}
