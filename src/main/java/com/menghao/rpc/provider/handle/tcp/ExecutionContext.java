package com.menghao.rpc.provider.handle.tcp;

import com.menghao.rpc.consumer.model.RpcRequest;
import com.menghao.rpc.netty.model.TcpConnection;
import com.menghao.rpc.provider.model.RpcResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * <p>一次调用执行上下文.</br>
 *
 * @author MarvelCode
 */
@AllArgsConstructor
public class ExecutionContext {

    @Getter
    private RpcRequest request;

    private TcpConnection connection;

    public void writeResult(Object result) {
        RpcResponse rpcResponse = new RpcResponse(request.getId(), result);
        connection.write(rpcResponse);
    }

    public void writeException(Throwable ex) {
        RpcResponse rpcResponse = new RpcResponse(request.getId(), ex);
        connection.write(rpcResponse);
    }
}
