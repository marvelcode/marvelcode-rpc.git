package com.menghao.rpc.provider.handle.tcp;

import com.menghao.rpc.consumer.model.RpcRequest;
import com.menghao.rpc.netty.TcpMessageHandler;
import com.menghao.rpc.netty.model.TcpConnection;
import com.menghao.rpc.spring.BeansManager;

/**
 * <p>RpcRequest处理类.</br>
 * <p>根据请求信息，定位服务，在 ExecutionExecutor中反射调用</p>
 *
 * @author MarvelCode
 */
public class RpcRequestMsgHandler implements TcpMessageHandler<RpcRequest> {

    @Override
    public boolean support(Class<?> supportClass) {
        return RpcRequest.class.isAssignableFrom(supportClass);
    }

    @Override
    public void handler(TcpConnection connection, RpcRequest data) {
        ExecutionExecutor executor = BeansManager.getInstance().getBeanByType(ExecutionExecutor.class);
        executor.execute(new ExecutionExecutor.ExecutionTask(
                new ExecutionContext(data, connection)
        ));
    }
}
