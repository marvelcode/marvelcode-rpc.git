package com.menghao.rpc.exception;

/**
 * <p>Rpc框架服务调用异常.</br>
 *
 * @author MarvelCode
 */
public class InvokeException extends RuntimeException {

    public InvokeException(String message) {
        super(message);
    }

    public InvokeException(Throwable cause) {
        super(cause);
    }
}
