package com.menghao.rpc.exception;

/**
 * <p>网络异常.<br>
 *
 * @author MarvelCode.
 */
public class NetException extends RuntimeException {

    public NetException(String message) {
        super(message);
    }

    public NetException(String message, Throwable cause) {
        super(message, cause);
    }
}
