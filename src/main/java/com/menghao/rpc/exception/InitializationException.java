package com.menghao.rpc.exception;

/**
 * <p>rpc框架初始化异常.</br>
 *
 * @author MarvelCode
 */
public class InitializationException extends RuntimeException {

    public InitializationException(String message) {
        super(message);
    }

    public InitializationException(NoSuchMethodException e) {
        super(e);
    }
}
