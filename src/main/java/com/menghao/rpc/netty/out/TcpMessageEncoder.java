package com.menghao.rpc.netty.out;

import com.menghao.rpc.serialize.ObjectOutput;
import com.menghao.rpc.serialize.hessian.HessianObjectOutput;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

import java.io.OutputStream;

/**
 * <p>Tcp信息编码类.</br>
 *
 * @author MarvelCode
 */
public class TcpMessageEncoder extends MessageToByteEncoder<Object> {

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, Object o, ByteBuf byteBuf) throws Exception {
        OutputStream outputStream = new ByteBufOutputStream(byteBuf);
        // 预占，回写包长度
        byteBuf.writeInt(0);
        ObjectOutput objectOutput = new HessianObjectOutput(outputStream);
        objectOutput.writeObject(o);
        objectOutput.flush();
        // 写包长度
        byteBuf.setInt(0, byteBuf.writerIndex() - 4);
    }
}
