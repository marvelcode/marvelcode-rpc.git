package com.menghao.rpc.netty;

import com.menghao.rpc.netty.model.TcpConnection;

import java.io.Serializable;

/**
 * <p>Tcp消息处理Handler.</br>
 *
 * @author MarvelCode
 */
public interface TcpMessageHandler<T> {

    /**
     * 判断该Handler是否支持指定类型的处理
     *
     * @param supportClass 支持处理的类型
     * @return 是否支持该处理
     */
    boolean support(Class<?> supportClass);

    /**
     * 处理读取到的数据
     *
     * @param connection 使用Netty Channel封装的连接
     * @param data       将要处理的数据
     */
    void handler(TcpConnection connection, T data);
}
