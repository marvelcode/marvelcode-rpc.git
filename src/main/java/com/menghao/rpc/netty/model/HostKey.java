package com.menghao.rpc.netty.model;

import lombok.AllArgsConstructor;

/**
 * <p>主机Key.<br>
 *
 * @author MarvelCode.
 */
@AllArgsConstructor
public class HostKey {

    private String ip;

    private int port;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HostKey hostKey = (HostKey) o;

        if (port != hostKey.port) return false;
        return ip != null ? ip.equals(hostKey.ip) : hostKey.ip == null;
    }

    @Override
    public int hashCode() {
        int result = ip != null ? ip.hashCode() : 0;
        result = 31 * result + port;
        return result;
    }
}
