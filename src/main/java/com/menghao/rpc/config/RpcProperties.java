package com.menghao.rpc.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <p>Rpc框架配置属性类.<br>
 *
 * @author MarvelCode.
 */
@Data
@ConfigurationProperties(prefix = "marvel.rpc")
public class RpcProperties {
    /**
     * zk服务地址
     */
    private String zkServerHost;

    /*
     * rpc框架调用类型
     */
    private static final String DEFAULT_TYPE = "http";
    private String type = DEFAULT_TYPE;


    private String zkRootPath = DEFAULT_ZK_PROVIDER_ROOT;
    private static final String DEFAULT_ZK_PROVIDER_ROOT = "marvel/rpc/providers";

    /**
     * 会话超时时间
     */
    private static final int DEFAULT_SESSION_TIMEOUT = 60000;
    private int sessionTimeout = DEFAULT_SESSION_TIMEOUT;

    /*
     * 连接超时时间
     */
    private static final int DEFAULT_CONNECT_TIMEOUT = 5000;
    private int connectionTimeout = DEFAULT_CONNECT_TIMEOUT;

    /**
     * 是否开启服务消费者模式
     */
    private static final boolean DEFAULT_CONSUMER_ENABLE = false;
    private boolean consumerEnable = DEFAULT_CONSUMER_ENABLE;

    /**
     * 是否开启服务提供者模式
     */
    private static final boolean DEFAULT_PROVIDER_ENABLE = false;
    private boolean providerEnable = DEFAULT_PROVIDER_ENABLE;
}
