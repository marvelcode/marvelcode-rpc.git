package com.menghao.rpc;

/**
 * <p>Cloud-Api常量类.<br>
 *
 * @author MarvelCode.
 */
public class RpcConstants {

    public static final String LOG_RPC_PREFIX = "marvel rpc:";
}
