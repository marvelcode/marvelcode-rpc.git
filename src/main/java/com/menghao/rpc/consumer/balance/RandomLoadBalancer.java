package com.menghao.rpc.consumer.balance;

import java.util.List;
import java.util.Random;

/**
 * <p>随机负载均衡.<br>
 *
 * @author MarvelCode.
 */
public class RandomLoadBalancer implements LoadBalancer {

    private Random random = new Random(System.currentTimeMillis());

    @Override
    public String select(List<String> ips) {
        return ips.get(random.nextInt(ips.size()));
    }

}
