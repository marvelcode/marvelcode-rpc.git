package com.menghao.rpc.consumer.balance;

import java.util.List;

/**
 * <p>负载均衡接口.<br>
 *
 * @author MarvelCode.
 */
public interface LoadBalancer {

    /**
     * 通过不同策略选取请求地址
     *
     * @param hosts 待选取主机列表
     * @return 经过选取策略后的主机
     */
    String select(List<String> hosts);
}
