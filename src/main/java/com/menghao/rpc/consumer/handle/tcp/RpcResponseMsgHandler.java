package com.menghao.rpc.consumer.handle.tcp;

import com.menghao.rpc.netty.TcpMessageHandler;
import com.menghao.rpc.netty.model.TcpConnection;
import com.menghao.rpc.provider.model.RpcResponse;
import com.menghao.rpc.spring.BeansManager;

/**
 * <p>RpcResponse处理类.</br>
 * <p>分析相应结果，调用 InvocationContext回写结果</p>
 *
 * @author MarvelCode
 */
public class RpcResponseMsgHandler implements TcpMessageHandler<RpcResponse> {

    @Override
    public boolean support(Class<?> supportClass) {
        return RpcResponse.class.isAssignableFrom(supportClass);
    }

    @Override
    public void handler(TcpConnection connection, RpcResponse data) {
        InvocationContextContainer contextContainer = BeansManager.getInstance().getBeanByType(InvocationContextContainer.class);
        InvocationContext invocationContext = contextContainer.get(data.getId());
        if (invocationContext != null) {
            invocationContext.notifyCompleted(data);
            contextContainer.remove(data.getId());
        }
    }
}
