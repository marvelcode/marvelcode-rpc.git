package com.menghao.rpc.consumer.handle.tcp;

import com.menghao.rpc.consumer.model.Future;
import com.menghao.rpc.consumer.model.RpcRequest;
import com.menghao.rpc.exception.InvokeException;
import com.menghao.rpc.netty.model.TcpConnection;
import com.menghao.rpc.provider.model.RpcResponse;
import com.menghao.rpc.spring.BeansManager;
import lombok.Getter;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>一次调用请求上下文.<br>
 * <p>存放请求及结果，伴随一次调用的生命周期</p>
 *
 * @author MarvelCode.
 */
public class InvocationContext implements Future {

    private InvocationContextContainer invocationContextContainer;

    @Getter
    private RpcRequest rpcRequest;

    private TcpConnection tcpConnection;

    /** 异步调用锁，在结果返回时解锁 */
    private Lock lock = new ReentrantLock();
    private Condition doneCondition = lock.newCondition();

    /** 预防调用无返回值的判断 */
    private static final Object NULL_OBJECT = new Object();

    private Object result;

    InvocationContext(TcpConnection tcpConnection, RpcRequest rpcRequest) {
        this.tcpConnection = tcpConnection;
        this.rpcRequest = rpcRequest;
        invocationContextContainer = BeansManager.getInstance().getBeanByType(InvocationContextContainer.class);
    }

    public void execute() {
        invocationContextContainer.add(this);
        tcpConnection.write(rpcRequest);
    }

    @Override
    public Object get() {
        if (!isDone()) {
            try {
                lock.lock();
                if (!isDone()) {
                    doneCondition.await();
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            } finally {
                lock.unlock();
            }
        }
        if (result instanceof Throwable) {
            throw new InvokeException((Throwable) result);
        }
        return result == NULL_OBJECT ? null : result;
    }

    @Override
    public boolean isDone() {
        return result != null;
    }

    public void notifyCompleted(RpcResponse rpcResponse) {
        lock.lock();
        try {
            if (rpcResponse.getThrowable() != null) {
                setResult(rpcResponse.getThrowable());
            }
            setResult(rpcResponse.getResult());
            doneCondition.signalAll();
        } finally {
            lock.unlock();
        }
    }

    private void setResult(Object value) {
        lock.lock();
        try {
            result = (value == null) ? NULL_OBJECT : value;
            doneCondition.signalAll();
        } finally {
            lock.unlock();
        }
        invocationContextContainer.remove(rpcRequest.getId());
    }
}
