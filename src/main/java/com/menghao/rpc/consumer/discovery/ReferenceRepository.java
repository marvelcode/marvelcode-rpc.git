package com.menghao.rpc.consumer.discovery;

import com.menghao.rpc.consumer.model.ReferenceKey;

/**
 * <p>@Reference仓库.</br>
 * <p>同一类型仅会初始化一次，需要填充服务提供方信息</p>
 *
 * @author MarvelCode
 * @see ReferenceRepository
 */
public interface ReferenceRepository {

    /**
     * 根据Key获取代理对象
     *
     * @param referenceKey 唯一标识代理对象的key
     * @return Object 代理对象
     */
    Object getReference(ReferenceKey referenceKey);
}
