package com.menghao.rpc.consumer.discovery.http;

import com.menghao.rpc.consumer.discovery.ProviderDiscovery;
import com.menghao.rpc.consumer.discovery.ReferenceRepository;
import com.menghao.rpc.consumer.handle.JdkProxyFactory;
import com.menghao.rpc.consumer.handle.ReferenceAgent;
import com.menghao.rpc.consumer.handle.http.HttpReferenceAgent;
import com.menghao.rpc.consumer.model.ReferenceKey;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * <p>Http方式代理对象仓库.</br>
 * <p>存放 ReferenceKey-HttpReferenceAgent 对应关系</p>
 * <p>作为HttpReferenceAgent的一级缓存，避免重复从ZooKeeper获取节点信息</p>
 *
 * @author MarvelCode
 */
public class HttpReferenceRepository implements ReferenceRepository {

    private JdkProxyFactory proxyFactory;

    private ProviderDiscovery providerDiscovery;

    private final ConcurrentMap<ReferenceKey, ReferenceAgent> referenceCache = new ConcurrentHashMap<>(8);

    public HttpReferenceRepository(JdkProxyFactory proxyFactory, ProviderDiscovery providerDiscovery) {
        this.proxyFactory = proxyFactory;
        this.providerDiscovery = providerDiscovery;
    }

    @Override
    public Object getReference(ReferenceKey referenceKey) {
        if (referenceCache.get(referenceKey) != null) {
            return proxyFactory.getProxy(referenceCache.get(referenceKey));
        }
        ReferenceAgent referenceAgent = new HttpReferenceAgent(referenceKey);
        // Http方式 @Reference 代理初始化
        providerDiscovery.initReferenceAgent(referenceAgent);
        referenceCache.putIfAbsent(referenceKey, referenceAgent);
        return proxyFactory.getProxy(referenceAgent);
    }

}
