package com.menghao.rpc.consumer.model;

/**
 * <p>异步调用接口.<br>
 *
 * @author MarvelCode.
 */
public interface Future {

    /*
     * 阻塞直到调用返回
     */
    Object get();

    /*
     * 判断调用是否结束
     */
    boolean isDone();
}
