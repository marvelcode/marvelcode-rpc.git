package com.menghao.rpc.consumer.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>Rpc统一请求封装.</br>
 * <p>包含了服务契约、调用方法名、入参类型、入参数组</p>
 *
 * @author MarvelCode
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RpcRequest implements Serializable {
    /**
     * 全局唯一Id，用于异步回写
     */
    private String id;
    /**
     * 同 implCode唯一标识一个服务
     */
    private String contract;
    /**
     * 同 contract唯一标识一个服务
     */
    private String implCode;
    /**
     * 服务对应的方法名
     */
    private String method;
    /**
     * 参数类型
     */
    private Class[] argsType;
    /**
     * 参数值
     */
    private Object[] args;
}
